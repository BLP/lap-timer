﻿namespace Lap_Timer
{
    partial class TimingDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBest = new System.Windows.Forms.Label();
            this.lblLast = new System.Windows.Forms.Label();
            this.lblLaps = new System.Windows.Forms.Label();
            this.lblLaneNumber = new System.Windows.Forms.Label();
            this.lblPlace = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBest
            // 
            this.lblBest.BackColor = System.Drawing.Color.Red;
            this.lblBest.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBest.Location = new System.Drawing.Point(470, 0);
            this.lblBest.Name = "lblBest";
            this.lblBest.Size = new System.Drawing.Size(145, 55);
            this.lblBest.TabIndex = 27;
            this.lblBest.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLast
            // 
            this.lblLast.BackColor = System.Drawing.Color.Red;
            this.lblLast.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLast.Location = new System.Drawing.Point(307, 0);
            this.lblLast.Name = "lblLast";
            this.lblLast.Size = new System.Drawing.Size(145, 55);
            this.lblLast.TabIndex = 26;
            this.lblLast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaps
            // 
            this.lblLaps.BackColor = System.Drawing.Color.Red;
            this.lblLaps.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLaps.Location = new System.Drawing.Point(190, 0);
            this.lblLaps.Name = "lblLaps";
            this.lblLaps.Size = new System.Drawing.Size(105, 55);
            this.lblLaps.TabIndex = 25;
            this.lblLaps.Text = "0";
            this.lblLaps.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLaneNumber
            // 
            this.lblLaneNumber.AutoSize = true;
            this.lblLaneNumber.BackColor = System.Drawing.Color.Red;
            this.lblLaneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLaneNumber.Location = new System.Drawing.Point(0, 0);
            this.lblLaneNumber.Name = "lblLaneNumber";
            this.lblLaneNumber.Size = new System.Drawing.Size(172, 55);
            this.lblLaneNumber.TabIndex = 24;
            this.lblLaneNumber.Text = "Lane 1";
            // 
            // lblPlace
            // 
            this.lblPlace.BackColor = System.Drawing.Color.Red;
            this.lblPlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlace.Location = new System.Drawing.Point(616, 0);
            this.lblPlace.Name = "lblPlace";
            this.lblPlace.Size = new System.Drawing.Size(78, 55);
            this.lblPlace.TabIndex = 44;
            this.lblPlace.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TimingDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.Controls.Add(this.lblPlace);
            this.Controls.Add(this.lblBest);
            this.Controls.Add(this.lblLast);
            this.Controls.Add(this.lblLaps);
            this.Controls.Add(this.lblLaneNumber);
            this.Name = "TimingDisplay";
            this.Size = new System.Drawing.Size(694, 55);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBest;
        private System.Windows.Forms.Label lblLast;
        private System.Windows.Forms.Label lblLaps;
        private System.Windows.Forms.Label lblLaneNumber;
        private System.Windows.Forms.Label lblPlace;
    }
}
