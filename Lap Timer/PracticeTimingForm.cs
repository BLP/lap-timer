﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SlimDX.DirectInput;

namespace Lap_Timer
{
    public partial class PracticeTimingForm : Form
    {
        Stopwatch raceTimer = new Stopwatch();
        PracticeLaneTimer[] Timers;
        TimingDisplay[] Displays = new TimingDisplay[4];
        Button[] ResetButtons = new Button[4];

        public PracticeTimingForm()
        {
            InitializeComponent();

            // Create the array for storing lane timers
            Timers = new PracticeLaneTimer[TimingSettings.NumLanes];

            // Put the timing displays into the array
            Displays[0] = Display1;
            Displays[1] = Display2;
            Displays[2] = Display3;
            Displays[3] = Display4;

            // Put the rest buttons into the array
            ResetButtons[0] = btnReset1;
            ResetButtons[1] = btnReset2;
            ResetButtons[2] = btnReset3;
            ResetButtons[3] = btnReset4;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Capture the joystick
            Joystick joystick = new Joystick(TimingSettings.Input, TimingSettings.TimingDevice.InstanceGuid);
            joystick.Acquire();
            joystick.Poll();

            // Start timing
            raceTimer.Start();

            // Enable the reset buttons
            btnReset1.Enabled = true;
            btnReset2.Enabled = true;
            btnReset3.Enabled = true;
            btnReset4.Enabled = true;

            // Commence timing on all of the lanes and set up the displays
            for (int i = 0; i < TimingSettings.NumLanes; i++)
            {
                // Create the lane timer and set it to the correct button
                Timers[i] = new PracticeLaneTimer(joystick, TimingSettings.LaneButtons[i], ref raceTimer, TimingSettings.MinimumLapTime);
                Timers[i].BeginRace();

                // Link the timer to the proper display
                Displays[i].LinkTimer(Timers[i]);

                // Set the display back color and visibility
                Displays[i].BackColor = TimingSettings.LaneColors[i];
                Displays[i].Visible = true;
                ResetButtons[i].Visible = true;
            }
        }

        private void btnReset1_Click(object sender, EventArgs e)
        {
            // Reset the lane
            Timers[0].ResetLane();
        }

        private void btnReset2_Click(object sender, EventArgs e)
        {
            // Reset the lane
            Timers[1].ResetLane();
        }

        private void btnReset3_Click(object sender, EventArgs e)
        {
            // Reset the lane
            Timers[2].ResetLane();
        }

        private void btnReset4_Click(object sender, EventArgs e)
        {
            // Reset the lane
            Timers[3].ResetLane();
        }
    }
}
