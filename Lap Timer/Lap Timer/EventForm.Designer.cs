﻿namespace Lap_Timer
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboEventType = new System.Windows.Forms.ComboBox();
            this.grpPracticeOptions = new System.Windows.Forms.GroupBox();
            this.grpLapsRaceOptions = new System.Windows.Forms.GroupBox();
            this.numLaps = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRace = new System.Windows.Forms.Button();
            this.grpTimedRaceOptions = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numSeconds = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numMinutes = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSettings = new System.Windows.Forms.Button();
            this.grpLapsRaceOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLaps)).BeginInit();
            this.grpTimedRaceOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Event Type:";
            // 
            // cboEventType
            // 
            this.cboEventType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEventType.FormattingEnabled = true;
            this.cboEventType.Items.AddRange(new object[] {
            "Practice",
            "Laps Race",
            "Timed Race"});
            this.cboEventType.Location = new System.Drawing.Point(116, 12);
            this.cboEventType.Name = "cboEventType";
            this.cboEventType.Size = new System.Drawing.Size(121, 21);
            this.cboEventType.TabIndex = 1;
            this.cboEventType.SelectedIndexChanged += new System.EventHandler(this.cboEventType_SelectedIndexChanged);
            // 
            // grpPracticeOptions
            // 
            this.grpPracticeOptions.Location = new System.Drawing.Point(12, 39);
            this.grpPracticeOptions.Name = "grpPracticeOptions";
            this.grpPracticeOptions.Size = new System.Drawing.Size(229, 66);
            this.grpPracticeOptions.TabIndex = 2;
            this.grpPracticeOptions.TabStop = false;
            this.grpPracticeOptions.Text = "Practice Options";
            this.grpPracticeOptions.Visible = false;
            // 
            // grpLapsRaceOptions
            // 
            this.grpLapsRaceOptions.Controls.Add(this.numLaps);
            this.grpLapsRaceOptions.Controls.Add(this.label2);
            this.grpLapsRaceOptions.Location = new System.Drawing.Point(12, 39);
            this.grpLapsRaceOptions.Name = "grpLapsRaceOptions";
            this.grpLapsRaceOptions.Size = new System.Drawing.Size(229, 66);
            this.grpLapsRaceOptions.TabIndex = 3;
            this.grpLapsRaceOptions.TabStop = false;
            this.grpLapsRaceOptions.Text = "Laps Race Options";
            this.grpLapsRaceOptions.Visible = false;
            // 
            // numLaps
            // 
            this.numLaps.Location = new System.Drawing.Point(93, 14);
            this.numLaps.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLaps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLaps.Name = "numLaps";
            this.numLaps.Size = new System.Drawing.Size(48, 20);
            this.numLaps.TabIndex = 1;
            this.numLaps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Number of laps:";
            // 
            // btnRace
            // 
            this.btnRace.Enabled = false;
            this.btnRace.Location = new System.Drawing.Point(166, 121);
            this.btnRace.Name = "btnRace";
            this.btnRace.Size = new System.Drawing.Size(75, 23);
            this.btnRace.TabIndex = 10;
            this.btnRace.Text = "Race!";
            this.btnRace.UseVisualStyleBackColor = true;
            this.btnRace.Click += new System.EventHandler(this.btnRace_Click);
            // 
            // grpTimedRaceOptions
            // 
            this.grpTimedRaceOptions.Controls.Add(this.label5);
            this.grpTimedRaceOptions.Controls.Add(this.numSeconds);
            this.grpTimedRaceOptions.Controls.Add(this.label4);
            this.grpTimedRaceOptions.Controls.Add(this.numMinutes);
            this.grpTimedRaceOptions.Controls.Add(this.label3);
            this.grpTimedRaceOptions.Location = new System.Drawing.Point(12, 39);
            this.grpTimedRaceOptions.Name = "grpTimedRaceOptions";
            this.grpTimedRaceOptions.Size = new System.Drawing.Size(229, 66);
            this.grpTimedRaceOptions.TabIndex = 11;
            this.grpTimedRaceOptions.TabStop = false;
            this.grpTimedRaceOptions.Text = "Timed Race Options";
            this.grpTimedRaceOptions.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(84, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "(secs)";
            // 
            // numSeconds
            // 
            this.numSeconds.Location = new System.Drawing.Point(124, 40);
            this.numSeconds.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numSeconds.Name = "numSeconds";
            this.numSeconds.Size = new System.Drawing.Size(48, 20);
            this.numSeconds.TabIndex = 8;
            this.numSeconds.ValueChanged += new System.EventHandler(this.numSeconds_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "(mins)";
            // 
            // numMinutes
            // 
            this.numMinutes.Location = new System.Drawing.Point(124, 14);
            this.numMinutes.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numMinutes.Name = "numMinutes";
            this.numMinutes.Size = new System.Drawing.Size(48, 20);
            this.numMinutes.TabIndex = 6;
            this.numMinutes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMinutes.ValueChanged += new System.EventHandler(this.numMinutes_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Race Length:";
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(8, 111);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(91, 35);
            this.btnSettings.TabIndex = 12;
            this.btnSettings.Text = "Timing and Display Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // EventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 158);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.grpTimedRaceOptions);
            this.Controls.Add(this.btnRace);
            this.Controls.Add(this.grpLapsRaceOptions);
            this.Controls.Add(this.grpPracticeOptions);
            this.Controls.Add(this.cboEventType);
            this.Controls.Add(this.label1);
            this.Name = "EventForm";
            this.Text = "Simple Lap Timer v1.0";
            this.grpLapsRaceOptions.ResumeLayout(false);
            this.grpLapsRaceOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLaps)).EndInit();
            this.grpTimedRaceOptions.ResumeLayout(false);
            this.grpTimedRaceOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinutes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEventType;
        private System.Windows.Forms.GroupBox grpPracticeOptions;
        private System.Windows.Forms.GroupBox grpLapsRaceOptions;
        private System.Windows.Forms.NumericUpDown numLaps;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRace;
        private System.Windows.Forms.GroupBox grpTimedRaceOptions;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numSeconds;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numMinutes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSettings;
    }
}