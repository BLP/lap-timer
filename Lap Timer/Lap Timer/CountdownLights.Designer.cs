﻿namespace Lap_Timer
{
    partial class CountdownLights
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblRed = new System.Windows.Forms.Label();
            this.lblYellow1 = new System.Windows.Forms.Label();
            this.lblYellow2 = new System.Windows.Forms.Label();
            this.lblGreen = new System.Windows.Forms.Label();
            this.tmrRed = new System.Windows.Forms.Timer(this.components);
            this.tmrYellow1 = new System.Windows.Forms.Timer(this.components);
            this.tmrYellow2 = new System.Windows.Forms.Timer(this.components);
            this.tmrGreen = new System.Windows.Forms.Timer(this.components);
            this.tmrEnd = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lblRed
            // 
            this.lblRed.BackColor = System.Drawing.Color.DarkRed;
            this.lblRed.Location = new System.Drawing.Point(3, 3);
            this.lblRed.Name = "lblRed";
            this.lblRed.Size = new System.Drawing.Size(25, 25);
            this.lblRed.TabIndex = 0;
            // 
            // lblYellow1
            // 
            this.lblYellow1.BackColor = System.Drawing.Color.Goldenrod;
            this.lblYellow1.Location = new System.Drawing.Point(34, 3);
            this.lblYellow1.Name = "lblYellow1";
            this.lblYellow1.Size = new System.Drawing.Size(25, 25);
            this.lblYellow1.TabIndex = 1;
            // 
            // lblYellow2
            // 
            this.lblYellow2.BackColor = System.Drawing.Color.Goldenrod;
            this.lblYellow2.Location = new System.Drawing.Point(65, 3);
            this.lblYellow2.Name = "lblYellow2";
            this.lblYellow2.Size = new System.Drawing.Size(25, 25);
            this.lblYellow2.TabIndex = 2;
            // 
            // lblGreen
            // 
            this.lblGreen.BackColor = System.Drawing.Color.DarkGreen;
            this.lblGreen.Location = new System.Drawing.Point(96, 3);
            this.lblGreen.Name = "lblGreen";
            this.lblGreen.Size = new System.Drawing.Size(25, 25);
            this.lblGreen.TabIndex = 3;
            // 
            // tmrRed
            // 
            this.tmrRed.Interval = 1000;
            this.tmrRed.Tick += new System.EventHandler(this.tmrRed_Tick);
            // 
            // tmrYellow1
            // 
            this.tmrYellow1.Interval = 1000;
            this.tmrYellow1.Tick += new System.EventHandler(this.tmrYellow1_Tick);
            // 
            // tmrYellow2
            // 
            this.tmrYellow2.Interval = 1000;
            this.tmrYellow2.Tick += new System.EventHandler(this.tmrYellow2_Tick);
            // 
            // tmrGreen
            // 
            this.tmrGreen.Interval = 1000;
            this.tmrGreen.Tick += new System.EventHandler(this.tmrGreen_Tick);
            // 
            // tmrEnd
            // 
            this.tmrEnd.Interval = 1000;
            this.tmrEnd.Tick += new System.EventHandler(this.tmrEnd_Tick);
            // 
            // CountdownLights
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.lblGreen);
            this.Controls.Add(this.lblYellow2);
            this.Controls.Add(this.lblYellow1);
            this.Controls.Add(this.lblRed);
            this.Name = "CountdownLights";
            this.Size = new System.Drawing.Size(124, 31);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRed;
        private System.Windows.Forms.Label lblYellow1;
        private System.Windows.Forms.Label lblYellow2;
        private System.Windows.Forms.Label lblGreen;
        private System.Windows.Forms.Timer tmrRed;
        private System.Windows.Forms.Timer tmrYellow1;
        private System.Windows.Forms.Timer tmrYellow2;
        private System.Windows.Forms.Timer tmrGreen;
        private System.Windows.Forms.Timer tmrEnd;
    }
}
