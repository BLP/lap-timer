﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SlimDX.DirectInput;

namespace Lap_Timer
{
    public partial class ConfigurationForm : Form
    {
        protected IList<DeviceInstance> DeviceInstances;
        Label[] LaneColors = new Label[4];
        NumericUpDown[] LaneButtons = new NumericUpDown[4];

        public ConfigurationForm()
        {
            InitializeComponent();
            cboAvailableJoysticks.TextChanged += new EventHandler(cboAvailableJoysticks_TextChanged);

            // Store the lane color labels in the array
            LaneColors[0] = lblLane1Color;
            LaneColors[1] = lblLane2Color;
            LaneColors[2] = lblLane3Color;
            LaneColors[3] = lblLane4Color;

            // Store the lane button selectors in the array
            LaneButtons[0] = numLane1Button;
            LaneButtons[1] = numLane2Button;
            LaneButtons[2] = numLane3Button;
            LaneButtons[3] = numLane4Button;
        }

        private void cboAvailableJoysticks_TextChanged(object sender, EventArgs e)
        {
            if (cboAvailableJoysticks.Text == string.Empty)
            {
                // Disable all of the button number selectors
                ChangeButtonSelectorsEnabled(false);
            }
            else
            {
                // Enable all of the button number selectors
                ChangeButtonSelectorsEnabled(true);
                
                // Update the maximum button number that can be selected
                UpdateButtonSelectors();
            }
        }

        private void btnChangeColor1_Click(object sender, EventArgs e)
        {
            // Set the current color in the dialog box
            cdbColorDialog.Color = lblLane1Color.BackColor;

            // Display the color dialog box
            if (cdbColorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Update the lane 1 color
                lblLane1Color.BackColor = cdbColorDialog.Color;
            }
        }

        private void btnChangeColor2_Click(object sender, EventArgs e)
        {
            // Set the current color in the dialog box
            cdbColorDialog.Color = lblLane2Color.BackColor;

            // Display the color dialog box
            if (cdbColorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Update the lane 1 color
                lblLane2Color.BackColor = cdbColorDialog.Color;
            }
        }

        private void btnChangeColor3_Click(object sender, EventArgs e)
        {
            // Set the current color in the dialog box
            cdbColorDialog.Color = lblLane3Color.BackColor;

            // Display the color dialog box
            if (cdbColorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Update the lane 1 color
                lblLane3Color.BackColor = cdbColorDialog.Color;
            }
        }

        private void btnChangeColor4_Click(object sender, EventArgs e)
        {
            // Set the current color in the dialog box
            cdbColorDialog.Color = lblLane4Color.BackColor;

            // Display the color dialog box
            if (cdbColorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Update the lane 1 color
                lblLane4Color.BackColor = cdbColorDialog.Color;
            }
        }

        private void SetLanes3And4Visibility(bool visible)
        {
            // Set the visibility of all lane 3 and 4 options
            lblLane3.Visible = visible;
            lblLane3Color.Visible = visible;
            btnChangeColor3.Visible = visible;
            lblButton3.Visible = visible;
            numLane3Button.Visible = visible;
            lblLane4.Visible = visible;
            lblLane4Color.Visible = visible;
            btnChangeColor4.Visible = visible;
            lblButton4.Visible = visible;
            numLane4Button.Visible = visible;
        }

        private void numLanes_ValueChanged(object sender, EventArgs e)
        {
            if (numLanes.Value == 2)
            {
                // There are only two lanes - hide the options for lanes 3 and 4
                SetLanes3And4Visibility(false);
            }
            else if (numLanes.Value == 4)
            {
                // There are four lanes - show the options for lanes 3 and 4
                SetLanes3And4Visibility(true);
            }
        }

        private void ScanForJoysticks()
        {
            // Find a joystick that is the timer input
            DeviceInstances = TimingSettings.Input.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AttachedOnly);
            
            // Clear out the device list and fill it with all of the discovered devices
            cboAvailableJoysticks.Items.Clear();
            foreach (DeviceInstance device in DeviceInstances)
            {
                cboAvailableJoysticks.Items.Add(device.InstanceName);
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            // Load a list of all available joysticks
            ScanForJoysticks();

            // Load the settings from the current settings
            numLanes.Value = TimingSettings.NumLanes;
            numMinLapTime.Value = (decimal)TimingSettings.MinimumLapTime.TotalSeconds;
            for (int i = 0; i < TimingSettings.NumLanes; i++)
            {
                LaneColors[i].BackColor = TimingSettings.LaneColors[i];
                LaneButtons[i].Value = TimingSettings.LaneButtons[i];
            }
            if (TimingSettings.TimingDevice != null)
                cboAvailableJoysticks.SelectedIndex = cboAvailableJoysticks.Items.IndexOf(TimingSettings.TimingDevice.InstanceName);
        }

        private void btnRefreshJoysticks_Click(object sender, EventArgs e)
        {
            // Reload the list of all available joysticks
            ScanForJoysticks();
        }

        private void UpdateButtonSelectors()
        {
                // Get the device capabilities
                Capabilities caps = new Joystick(TimingSettings.Input,
                    DeviceInstances[cboAvailableJoysticks.SelectedIndex].InstanceGuid).Capabilities;

                // Set the button maximum for all four lanes
                numLane1Button.Maximum = caps.ButtonCount - 1;
                numLane2Button.Maximum = caps.ButtonCount - 1;
                numLane3Button.Maximum = caps.ButtonCount - 1;
                numLane4Button.Maximum = caps.ButtonCount - 1;
        }

        private void ChangeButtonSelectorsEnabled(bool enabled)
        {
            numLane1Button.Enabled = enabled;
            numLane2Button.Enabled = enabled;
            numLane3Button.Enabled = enabled;
            numLane4Button.Enabled = enabled;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            // Create and set the timing settings
            if (DeviceInstances.Count > 0 && cboAvailableJoysticks.SelectedIndex != -1)
            {
                TimingSettings.TimingDevice = DeviceInstances[cboAvailableJoysticks.SelectedIndex];
            }
            TimingSettings.NumLanes = (int)numLanes.Value;
            TimingSettings.MinimumLapTime = TimeSpan.FromSeconds((double)numMinLapTime.Value);
            for (int i = 0; i < TimingSettings.NumLanes; i++)
            {
                TimingSettings.LaneColors[i] = LaneColors[i].BackColor;
                TimingSettings.LaneButtons[i] = (int)LaneButtons[i].Value;
            }

            // Close this form
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Close the form
            this.Close();
        }
    }
}
