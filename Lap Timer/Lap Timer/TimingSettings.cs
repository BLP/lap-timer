﻿using System;
using System.Drawing;
using SlimDX.DirectInput;

public static class TimingSettings
{
    public static Color[] LaneColors = { Color.Red, Color.Blue, Color.White, Color.Yellow };
    public static int[] LaneButtons = { 0, 1, 2, 3 };
    public static DeviceInstance TimingDevice;
    public static TimeSpan MinimumLapTime = TimeSpan.FromSeconds(1);
    private static int NumberOfLanes = 4;
    public static int NumLanes
    {
        get
        {
            return NumberOfLanes;
        }
        set
        {
            // Set the size of the lane color and button arrays, saving their contents
            Color[] newColors = new Color[value];
            for (int i = 0; i < Math.Min(value, NumberOfLanes); i++)
            {
                newColors[i] = LaneColors[i];
            }
            int[] newButtons = new int[value];
            for (int i = 0; i < Math.Min(value, NumberOfLanes); i++)
            {
                newButtons[i] = LaneButtons[i];
            }
            LaneColors = newColors;
            LaneButtons = newButtons;

            // Set the value for the numbe of lanes
            NumberOfLanes = value;
        }
    }
    public static DirectInput Input = new DirectInput();
}

