﻿namespace Lap_Timer
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numMinLapTime = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numLanes = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnChangeColor4 = new System.Windows.Forms.Button();
            this.lblLane4Color = new System.Windows.Forms.Label();
            this.lblLane4 = new System.Windows.Forms.Label();
            this.btnChangeColor3 = new System.Windows.Forms.Button();
            this.lblLane3Color = new System.Windows.Forms.Label();
            this.lblLane3 = new System.Windows.Forms.Label();
            this.btnChangeColor2 = new System.Windows.Forms.Button();
            this.lblLane2Color = new System.Windows.Forms.Label();
            this.lblLane2 = new System.Windows.Forms.Label();
            this.btnChangeColor1 = new System.Windows.Forms.Button();
            this.lblLane1Color = new System.Windows.Forms.Label();
            this.lblLane1 = new System.Windows.Forms.Label();
            this.cdbColorDialog = new System.Windows.Forms.ColorDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numLane4Button = new System.Windows.Forms.NumericUpDown();
            this.lblButton4 = new System.Windows.Forms.Label();
            this.numLane3Button = new System.Windows.Forms.NumericUpDown();
            this.lblButton3 = new System.Windows.Forms.Label();
            this.numLane2Button = new System.Windows.Forms.NumericUpDown();
            this.lblButton2 = new System.Windows.Forms.Label();
            this.numLane1Button = new System.Windows.Forms.NumericUpDown();
            this.lblButton1 = new System.Windows.Forms.Label();
            this.btnRefreshJoysticks = new System.Windows.Forms.Button();
            this.cboAvailableJoysticks = new System.Windows.Forms.ComboBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLapTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLanes)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLane4Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane3Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane2Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane1Button)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numMinLapTime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numLanes);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(171, 71);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timing Settings";
            // 
            // numMinLapTime
            // 
            this.numMinLapTime.DecimalPlaces = 1;
            this.numMinLapTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numMinLapTime.Location = new System.Drawing.Point(110, 40);
            this.numMinLapTime.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinLapTime.Name = "numMinLapTime";
            this.numMinLapTime.Size = new System.Drawing.Size(50, 20);
            this.numMinLapTime.TabIndex = 5;
            this.numMinLapTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Minimum Lap Time:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Number of Lanes:";
            // 
            // numLanes
            // 
            this.numLanes.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numLanes.Location = new System.Drawing.Point(103, 14);
            this.numLanes.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLanes.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numLanes.Name = "numLanes";
            this.numLanes.ReadOnly = true;
            this.numLanes.Size = new System.Drawing.Size(35, 20);
            this.numLanes.TabIndex = 2;
            this.numLanes.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numLanes.ValueChanged += new System.EventHandler(this.numLanes_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.Controls.Add(this.btnChangeColor4);
            this.groupBox2.Controls.Add(this.lblLane4Color);
            this.groupBox2.Controls.Add(this.lblLane4);
            this.groupBox2.Controls.Add(this.btnChangeColor3);
            this.groupBox2.Controls.Add(this.lblLane3Color);
            this.groupBox2.Controls.Add(this.lblLane3);
            this.groupBox2.Controls.Add(this.btnChangeColor2);
            this.groupBox2.Controls.Add(this.lblLane2Color);
            this.groupBox2.Controls.Add(this.lblLane2);
            this.groupBox2.Controls.Add(this.btnChangeColor1);
            this.groupBox2.Controls.Add(this.lblLane1Color);
            this.groupBox2.Controls.Add(this.lblLane1);
            this.groupBox2.Location = new System.Drawing.Point(12, 89);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(171, 136);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Display Settings";
            // 
            // btnChangeColor4
            // 
            this.btnChangeColor4.Location = new System.Drawing.Point(101, 98);
            this.btnChangeColor4.Name = "btnChangeColor4";
            this.btnChangeColor4.Size = new System.Drawing.Size(59, 23);
            this.btnChangeColor4.TabIndex = 11;
            this.btnChangeColor4.Text = "Change";
            this.btnChangeColor4.UseVisualStyleBackColor = true;
            this.btnChangeColor4.Click += new System.EventHandler(this.btnChangeColor4_Click);
            // 
            // lblLane4Color
            // 
            this.lblLane4Color.BackColor = System.Drawing.Color.Yellow;
            this.lblLane4Color.Location = new System.Drawing.Point(82, 103);
            this.lblLane4Color.Name = "lblLane4Color";
            this.lblLane4Color.Size = new System.Drawing.Size(13, 13);
            this.lblLane4Color.TabIndex = 10;
            // 
            // lblLane4
            // 
            this.lblLane4.AutoSize = true;
            this.lblLane4.Location = new System.Drawing.Point(6, 103);
            this.lblLane4.Name = "lblLane4";
            this.lblLane4.Size = new System.Drawing.Size(70, 13);
            this.lblLane4.TabIndex = 9;
            this.lblLane4.Text = "Lane 4 Color:";
            // 
            // btnChangeColor3
            // 
            this.btnChangeColor3.Location = new System.Drawing.Point(101, 69);
            this.btnChangeColor3.Name = "btnChangeColor3";
            this.btnChangeColor3.Size = new System.Drawing.Size(59, 23);
            this.btnChangeColor3.TabIndex = 8;
            this.btnChangeColor3.Text = "Change";
            this.btnChangeColor3.UseVisualStyleBackColor = true;
            this.btnChangeColor3.Click += new System.EventHandler(this.btnChangeColor3_Click);
            // 
            // lblLane3Color
            // 
            this.lblLane3Color.BackColor = System.Drawing.Color.White;
            this.lblLane3Color.Location = new System.Drawing.Point(82, 74);
            this.lblLane3Color.Name = "lblLane3Color";
            this.lblLane3Color.Size = new System.Drawing.Size(13, 13);
            this.lblLane3Color.TabIndex = 7;
            // 
            // lblLane3
            // 
            this.lblLane3.AutoSize = true;
            this.lblLane3.Location = new System.Drawing.Point(6, 74);
            this.lblLane3.Name = "lblLane3";
            this.lblLane3.Size = new System.Drawing.Size(70, 13);
            this.lblLane3.TabIndex = 6;
            this.lblLane3.Text = "Lane 3 Color:";
            // 
            // btnChangeColor2
            // 
            this.btnChangeColor2.Location = new System.Drawing.Point(101, 40);
            this.btnChangeColor2.Name = "btnChangeColor2";
            this.btnChangeColor2.Size = new System.Drawing.Size(59, 23);
            this.btnChangeColor2.TabIndex = 5;
            this.btnChangeColor2.Text = "Change";
            this.btnChangeColor2.UseVisualStyleBackColor = true;
            this.btnChangeColor2.Click += new System.EventHandler(this.btnChangeColor2_Click);
            // 
            // lblLane2Color
            // 
            this.lblLane2Color.BackColor = System.Drawing.Color.Blue;
            this.lblLane2Color.Location = new System.Drawing.Point(82, 45);
            this.lblLane2Color.Name = "lblLane2Color";
            this.lblLane2Color.Size = new System.Drawing.Size(13, 13);
            this.lblLane2Color.TabIndex = 4;
            // 
            // lblLane2
            // 
            this.lblLane2.AutoSize = true;
            this.lblLane2.Location = new System.Drawing.Point(6, 45);
            this.lblLane2.Name = "lblLane2";
            this.lblLane2.Size = new System.Drawing.Size(70, 13);
            this.lblLane2.TabIndex = 3;
            this.lblLane2.Text = "Lane 2 Color:";
            // 
            // btnChangeColor1
            // 
            this.btnChangeColor1.Location = new System.Drawing.Point(101, 11);
            this.btnChangeColor1.Name = "btnChangeColor1";
            this.btnChangeColor1.Size = new System.Drawing.Size(59, 23);
            this.btnChangeColor1.TabIndex = 2;
            this.btnChangeColor1.Text = "Change";
            this.btnChangeColor1.UseVisualStyleBackColor = true;
            this.btnChangeColor1.Click += new System.EventHandler(this.btnChangeColor1_Click);
            // 
            // lblLane1Color
            // 
            this.lblLane1Color.BackColor = System.Drawing.Color.Red;
            this.lblLane1Color.Location = new System.Drawing.Point(82, 16);
            this.lblLane1Color.Name = "lblLane1Color";
            this.lblLane1Color.Size = new System.Drawing.Size(13, 13);
            this.lblLane1Color.TabIndex = 1;
            // 
            // lblLane1
            // 
            this.lblLane1.AutoSize = true;
            this.lblLane1.Location = new System.Drawing.Point(6, 16);
            this.lblLane1.Name = "lblLane1";
            this.lblLane1.Size = new System.Drawing.Size(70, 13);
            this.lblLane1.TabIndex = 0;
            this.lblLane1.Text = "Lane 1 Color:";
            // 
            // cdbColorDialog
            // 
            this.cdbColorDialog.AnyColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numLane4Button);
            this.groupBox3.Controls.Add(this.lblButton4);
            this.groupBox3.Controls.Add(this.numLane3Button);
            this.groupBox3.Controls.Add(this.lblButton3);
            this.groupBox3.Controls.Add(this.numLane2Button);
            this.groupBox3.Controls.Add(this.lblButton2);
            this.groupBox3.Controls.Add(this.numLane1Button);
            this.groupBox3.Controls.Add(this.lblButton1);
            this.groupBox3.Controls.Add(this.btnRefreshJoysticks);
            this.groupBox3.Controls.Add(this.cboAvailableJoysticks);
            this.groupBox3.Location = new System.Drawing.Point(205, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(276, 164);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input Settings";
            // 
            // numLane4Button
            // 
            this.numLane4Button.Enabled = false;
            this.numLane4Button.Location = new System.Drawing.Point(86, 132);
            this.numLane4Button.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.numLane4Button.Name = "numLane4Button";
            this.numLane4Button.Size = new System.Drawing.Size(41, 20);
            this.numLane4Button.TabIndex = 9;
            this.numLane4Button.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // lblButton4
            // 
            this.lblButton4.AutoSize = true;
            this.lblButton4.Location = new System.Drawing.Point(3, 134);
            this.lblButton4.Name = "lblButton4";
            this.lblButton4.Size = new System.Drawing.Size(77, 13);
            this.lblButton4.TabIndex = 8;
            this.lblButton4.Text = "Lane 4 Button:";
            // 
            // numLane3Button
            // 
            this.numLane3Button.Enabled = false;
            this.numLane3Button.Location = new System.Drawing.Point(86, 105);
            this.numLane3Button.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.numLane3Button.Name = "numLane3Button";
            this.numLane3Button.Size = new System.Drawing.Size(41, 20);
            this.numLane3Button.TabIndex = 7;
            this.numLane3Button.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblButton3
            // 
            this.lblButton3.AutoSize = true;
            this.lblButton3.Location = new System.Drawing.Point(3, 107);
            this.lblButton3.Name = "lblButton3";
            this.lblButton3.Size = new System.Drawing.Size(77, 13);
            this.lblButton3.TabIndex = 6;
            this.lblButton3.Text = "Lane 3 Button:";
            // 
            // numLane2Button
            // 
            this.numLane2Button.Enabled = false;
            this.numLane2Button.Location = new System.Drawing.Point(86, 78);
            this.numLane2Button.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.numLane2Button.Name = "numLane2Button";
            this.numLane2Button.Size = new System.Drawing.Size(41, 20);
            this.numLane2Button.TabIndex = 5;
            this.numLane2Button.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblButton2
            // 
            this.lblButton2.AutoSize = true;
            this.lblButton2.Location = new System.Drawing.Point(3, 80);
            this.lblButton2.Name = "lblButton2";
            this.lblButton2.Size = new System.Drawing.Size(77, 13);
            this.lblButton2.TabIndex = 4;
            this.lblButton2.Text = "Lane 2 Button:";
            // 
            // numLane1Button
            // 
            this.numLane1Button.Enabled = false;
            this.numLane1Button.Location = new System.Drawing.Point(86, 51);
            this.numLane1Button.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.numLane1Button.Name = "numLane1Button";
            this.numLane1Button.Size = new System.Drawing.Size(41, 20);
            this.numLane1Button.TabIndex = 3;
            // 
            // lblButton1
            // 
            this.lblButton1.AutoSize = true;
            this.lblButton1.Location = new System.Drawing.Point(3, 53);
            this.lblButton1.Name = "lblButton1";
            this.lblButton1.Size = new System.Drawing.Size(77, 13);
            this.lblButton1.TabIndex = 2;
            this.lblButton1.Text = "Lane 1 Button:";
            // 
            // btnRefreshJoysticks
            // 
            this.btnRefreshJoysticks.Location = new System.Drawing.Point(195, 17);
            this.btnRefreshJoysticks.Name = "btnRefreshJoysticks";
            this.btnRefreshJoysticks.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshJoysticks.TabIndex = 1;
            this.btnRefreshJoysticks.Text = "Refresh";
            this.btnRefreshJoysticks.UseVisualStyleBackColor = true;
            this.btnRefreshJoysticks.Click += new System.EventHandler(this.btnRefreshJoysticks_Click);
            // 
            // cboAvailableJoysticks
            // 
            this.cboAvailableJoysticks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAvailableJoysticks.FormattingEnabled = true;
            this.cboAvailableJoysticks.Location = new System.Drawing.Point(6, 19);
            this.cboAvailableJoysticks.Name = "cboAvailableJoysticks";
            this.cboAvailableJoysticks.Size = new System.Drawing.Size(183, 21);
            this.cboAvailableJoysticks.TabIndex = 0;
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(325, 198);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 10;
            this.btnAccept.Text = "OK";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(406, 198);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 237);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ConfigurationForm";
            this.Text = "Configure Lap Timer";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLapTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLanes)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLane4Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane3Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane2Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLane1Button)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numMinLapTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numLanes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblLane1Color;
        private System.Windows.Forms.Label lblLane1;
        private System.Windows.Forms.Button btnChangeColor1;
        private System.Windows.Forms.ColorDialog cdbColorDialog;
        private System.Windows.Forms.Button btnChangeColor2;
        private System.Windows.Forms.Label lblLane2Color;
        private System.Windows.Forms.Label lblLane2;
        private System.Windows.Forms.Button btnChangeColor4;
        private System.Windows.Forms.Label lblLane4Color;
        private System.Windows.Forms.Label lblLane4;
        private System.Windows.Forms.Button btnChangeColor3;
        private System.Windows.Forms.Label lblLane3Color;
        private System.Windows.Forms.Label lblLane3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRefreshJoysticks;
        private System.Windows.Forms.ComboBox cboAvailableJoysticks;
        private System.Windows.Forms.NumericUpDown numLane1Button;
        private System.Windows.Forms.Label lblButton1;
        private System.Windows.Forms.NumericUpDown numLane4Button;
        private System.Windows.Forms.Label lblButton4;
        private System.Windows.Forms.NumericUpDown numLane3Button;
        private System.Windows.Forms.Label lblButton3;
        private System.Windows.Forms.NumericUpDown numLane2Button;
        private System.Windows.Forms.Label lblButton2;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;

    }
}