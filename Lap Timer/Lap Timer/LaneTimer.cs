﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.DirectInput;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace Lap_Timer
{
    public class LaneTimer
    {
        public event EventHandler Reset;
        public event EventHandler TimingBegun;
        public event EventHandler TimingCancelled;
        public event EventHandler<LapCompletedEventArgs> LapCompleted;
        public event EventHandler<RaceCompletedEventArgs> RaceCompleted;
        public bool RaceOver { get; protected set; }
        protected Joystick TimerInput;
        //protected BackgroundWorker Worker = new BackgroundWorker();
        protected Thread TimingThread;
        protected TimeSpan LastTime;
        protected Stopwatch RaceTime;
        protected TimeSpan MinLapTime;
        public int NumLaps { get; protected set; }
        public TimeSpan BestLap { get; protected set; }
        public TimeSpan TotalTime { get; protected set; }
        protected TimeSpan LastLap;
        protected int ButtonNumber;
        protected Func<TimeSpan, int, bool> RaceOverTest;

        public LaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime, Func<TimeSpan, int, bool> raceOverTest)
            : this(timerInput, buttonNumber, ref raceTime, raceOverTest, TimeSpan.FromSeconds(1))
        {
        }

        public LaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime, Func<TimeSpan, int, bool> raceOverTest, TimeSpan minLapTime)
        {
            // Set the joystick input, button number, race timer, and minimum lap time
            TimerInput = timerInput;
            ButtonNumber = buttonNumber;
            MinLapTime = minLapTime;
            RaceTime = raceTime;
            RaceOverTest = raceOverTest;

            // Set the best time to the maximum
            BestLap = TimeSpan.MaxValue;

            // Create the background worker
            //Worker.DoWork += new DoWorkEventHandler(UpdateLane);
            //Worker.WorkerSupportsCancellation = true;
            //TimingThread = new Thread(UpdateLane);
        }

        /*public void SetTimer(Stopwatch raceTimer)
        {
            // Set the race time
            RaceTime = raceTimer;
        }*/

        public void BeginRace()
        {
            //if (!Worker.IsBusy)
            // Set the background worker to work if it isn't already
            //Worker.RunWorkerAsync();
            TimingThread = new Thread(UpdateLane);
            TimingThread.Start();

            // Notify everyone that timing is beginnng
            if (TimingBegun != null)
                TimingBegun(this, new EventArgs());
        }

        public virtual void ResetLane()
        {
            // Create the background worker
            /*Worker = new BackgroundWorker();
            Worker.DoWork += new DoWorkEventHandler(UpdateLane);
            Worker.WorkerSupportsCancellation = true;*/

            // Reset the info
            LastLap = TimeSpan.Zero;
            BestLap = TimeSpan.MaxValue;
            NumLaps = 0;

            // Notify everyone that a reset has occurred
            if (Reset != null)
                Reset(this, new EventArgs());
        }

        public void CancelRace()
        {
            if (TimingThread != null && TimingThread.ThreadState == System.Threading.ThreadState.Running)
            {
                // Stop the background worker from working
                //Worker.CancelAsync();
                TimingThread.Abort();
            }
        }

        public void EndRace()
        {
            //if (Worker != null && Worker.IsBusy)
            if (TimingThread != null && TimingThread.ThreadState == System.Threading.ThreadState.Running)
            {
                // Stop the background worker from working
                //Worker.CancelAsync();
                TimingThread.Abort();

                // Notify everyone that timing has been cancelled
                if (TimingCancelled != null)
                    TimingCancelled(this, new EventArgs());
            }
        }

        protected void InvokeLapCompleted(LapCompletedEventArgs e)
        {
            // Invoke the lap completed event
            if (LapCompleted != null)
                LapCompleted(this, e);
        }

        protected void InvokeRaceCompleted(RaceCompletedEventArgs e)
        {
            // Invoke the race completed event
            if (RaceCompleted != null)
                RaceCompleted(this, e);
        }

        //protected void UpdateLane(object sender, DoWorkEventArgs e)
        protected void UpdateLane()
        {
            // Poll the joystick
            TimerInput.Poll();

            // Initialize joystick state
            JoystickState oldState = TimerInput.GetCurrentState();
            JoystickState newState;

            // Wait for the car to cross the line for the first time
            while (TimerInput.GetCurrentState().IsReleased(ButtonNumber) == oldState.IsReleased(ButtonNumber) /*|| Worker.CancellationPending*/)
            {
                // Store the old joystick state
                oldState = TimerInput.GetCurrentState();

                // Poll the joystick
                TimerInput.Poll();
            }

            // Store the current time as the last time
            LastTime = RaceTime.Elapsed;

            // Loop until the race is finished, checking for the car to cross the line
            //while (!Worker.CancellationPending)
            while (true)
            {
                // Poll the joystick
                TimerInput.Poll();

                // Get the current race time and the current joystick state
                TimeSpan currentTime = RaceTime.Elapsed;
                newState = TimerInput.GetCurrentState();

                if (newState.IsReleased(ButtonNumber) && oldState.IsPressed(ButtonNumber))
                {
                    // A car is newly crossing the line - calculate the lap time
                    TimeSpan lapTime = currentTime - LastTime;
                    if (lapTime > MinLapTime)
                    {
                        // The lap time isn't too short - a lap has been completed
                        NumLaps++;
                        LastLap = lapTime;

                        // Check for a best time
                        if (lapTime < BestLap)
                        {
                            // A new best time! Store it
                            BestLap = lapTime;

                            // Let any interested parties know that a new lap has been completed with a best time
                            InvokeLapCompleted(new LapCompletedEventArgs(NumLaps, LastLap, true));
                        }
                        else
                        {
                            // Let any interested parties know that a new lap has been completed without a best time
                            InvokeLapCompleted(new LapCompletedEventArgs(NumLaps, LastLap, false));
                        }

                        // Check to see if the race has been completed
                        if (RaceOverTest(currentTime, NumLaps))
                        {
                            // The race has been completed - fire the event
                            RaceOver = true;
                            TotalTime = currentTime;
                            InvokeRaceCompleted(new RaceCompletedEventArgs(NumLaps, currentTime, BestLap));

                            // Break out of the loop
                            break;
                        }

                        // Store the current time as the last time
                        LastTime = currentTime;
                    }
                }

                // Store the previous joystick state
                oldState = newState;
            }

            //e.Cancel = true;
            // We're finished with timing, so dispose the worker
            //Worker.Dispose();
        }
    }

    public class RaceCompletedEventArgs : EventArgs
    {
        public int LapsCompleted { get; protected set; }
        public TimeSpan RaceTime { get; protected set; }
        public TimeSpan BestTime { get; protected set; }

        public RaceCompletedEventArgs(int lapsCompleted, TimeSpan raceTime, TimeSpan bestTime)
        {
            LapsCompleted = lapsCompleted;
            RaceTime = raceTime;
            BestTime = bestTime;
        }
    }

    public class LapCompletedEventArgs : EventArgs
    {
        public int LapsCompleted { get; protected set; }
        public TimeSpan LapTime { get; protected set; }
        public bool BestTime { get; protected set; }

        public LapCompletedEventArgs(int lapsCompleted, TimeSpan lapTime, bool bestTime)
        {
            LapsCompleted = lapsCompleted;
            LapTime = lapTime;
            BestTime = bestTime;
        }
    }
}