﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lap_Timer
{
    public partial class EventForm : Form
    {
        // Define constants to represent each type of event
        const int PracticeEvent = 0;
        const int LapsRaceEvent = 1;
        const int TimedRaceEvent = 2;

        public EventForm()
        {
            InitializeComponent();
        }

        private void cboEventType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Select which option group box to show based upon what type of event is selected
            switch (cboEventType.SelectedIndex)
            {
                case PracticeEvent:
                    // Show the Practice options and hide the others
                    grpPracticeOptions.Visible = true;
                    grpLapsRaceOptions.Visible = false;
                    grpTimedRaceOptions.Visible = false;
                    // Enable the race button
                    btnRace.Enabled = true;
                    break;
                case LapsRaceEvent:
                    // Show the Laps Race options and hide the others
                    grpPracticeOptions.Visible = false;
                    grpLapsRaceOptions.Visible = true;
                    grpTimedRaceOptions.Visible = false;
                    // Enable the race button
                    btnRace.Enabled = true;
                    break;
                case TimedRaceEvent:
                    // Show the Timed Race options and hide the others
                    grpPracticeOptions.Visible = false;
                    grpLapsRaceOptions.Visible = false;
                    grpTimedRaceOptions.Visible = true;
                    // Enable the race button
                    btnRace.Enabled = true;
                    break;
                default:
                    // Hide all of the options
                    grpPracticeOptions.Visible = false;
                    grpLapsRaceOptions.Visible = false;
                    grpTimedRaceOptions.Visible = false;
                    // Disable the race button
                    btnRace.Enabled = false;
                    break;
            }
        }

        private void btnRace_Click(object sender, EventArgs e)
        {
            // Check to make sure a joystick device is selected
            if (TimingSettings.TimingDevice != null)
            {
                // Display the appropriate timing form based on the type of event selected
                switch (cboEventType.SelectedIndex)
                {
                    case PracticeEvent:
                        // Hide this form
                        this.Hide();

                        // Create and display a PracticeTimingForm
                        PracticeTimingForm frmPractice = new PracticeTimingForm();
                        frmPractice.ShowDialog();

                        // Show this form
                        this.Show();
                        break;
                    case LapsRaceEvent:
                        // Hide this form
                        this.Hide();

                        // Create and display a LapsRaceTimingForm
                        LapsRaceTimingForm frmLapsRace = new LapsRaceTimingForm((int)numLaps.Value);
                        frmLapsRace.ShowDialog();

                        // Show this form
                        this.Show();
                        break;
                    case TimedRaceEvent:
                        // Hide this form
                        this.Hide();

                        // Create and display a TimedRaceTimingForm
                        TimedRaceTimingForm frmTimedRace = new TimedRaceTimingForm(TimeSpan.FromSeconds((double)numSeconds.Value) + 
                            TimeSpan.FromMinutes((double)numMinutes.Value));
                        frmTimedRace.ShowDialog();

                        // Show this form
                        this.Show();
                        break;
                }
            }
            else
            {
                // Let the user know that they need to select a timing device
                MessageBox.Show("You must select a timing input device using the Timing and Display Settings before you can race.");
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            // Create and display a configuration form
            ConfigurationForm frmConfiguration = new ConfigurationForm();
            frmConfiguration.ShowDialog();
        }

        private void numSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (numSeconds.Value == 60)
            {
                // The number of seconds is 60. Change it to 0 and add 1 minute.
                numMinutes.Value++;
                numSeconds.Value = 0;
            }
            else if (numMinutes.Value == 0 && numSeconds.Value == 0)
            {
                // Make sure the time is never 0
                numSeconds.Value = 1;
            }
        }

        private void numMinutes_ValueChanged(object sender, EventArgs e)
        {
            if (numMinutes.Value == 0 && numSeconds.Value == 0)
            {
                // Make sure the timer is never 0
                numSeconds.Value = 1;
            }
        }
    }
}
