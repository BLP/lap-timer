﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lap_Timer
{
    public partial class CountdownLights : UserControl
    {
        public event EventHandler CountdownFinished;

        public bool Counting { get; protected set; }

        public CountdownLights()
        {
            InitializeComponent();
        }

        public void BeginCounting()
        {
            // Rest lights
            ResetLights();

            // Begin the couting sequence
            tmrRed.Enabled = true;

            // We are counting
            Counting = true;
        }

        public void CancelCounting()
        {
            // Disable all of the timers
            tmrRed.Enabled = false;
            tmrYellow1.Enabled = false;
            tmrYellow2.Enabled = false;
            tmrGreen.Enabled = false;
            tmrEnd.Enabled = false;

            // Reset all of the lights
            ResetLights();
            
            // We are not counting
            Counting = false;
        }

        protected void ResetLights()
        {
            lblRed.BackColor = Color.DarkRed;
            lblYellow1.BackColor = Color.Goldenrod;
            lblYellow2.BackColor = Color.Goldenrod;
            lblGreen.BackColor = Color.DarkGreen;
        }

        public void LightAll()
        {
            lblRed.BackColor = Color.Red;
            lblYellow1.BackColor = Color.Yellow;
            lblYellow2.BackColor = Color.Yellow;
            lblGreen.BackColor = Color.Lime;
        }

        private void tmrRed_Tick(object sender, EventArgs e)
        {
            // Turn on the red light
            lblRed.BackColor = Color.Red;

            // Enable the next timer in the sequence
            tmrYellow1.Enabled = true;

            // Disable this timer
            tmrRed.Enabled = false;
        }

        private void tmrYellow1_Tick(object sender, EventArgs e)
        {
            // Turn on the first yellow light
            lblYellow1.BackColor = Color.Yellow;

            // Enable the next timer in the sequence
            tmrYellow2.Enabled = true;

            // Disable this timer
            tmrYellow1.Enabled = false;
        }

        private void tmrYellow2_Tick(object sender, EventArgs e)
        {
            // Turn on the second yellow light
            lblYellow2.BackColor = Color.Yellow;

            // Enable the next timer in the sequence
            tmrGreen.Enabled = true;

            // Disable this timer
            tmrYellow2.Enabled = false;
        }

        private void tmrGreen_Tick(object sender, EventArgs e)
        {
            // Turn on the green light
            lblGreen.BackColor = Color.Lime;

            // Fire the countdown finished event
            CountdownFinished(this, new EventArgs());

            // Turn on the final timer in the sequence
            tmrEnd.Enabled = true;

            // Disable this timer
            tmrGreen.Enabled = false;
        }

        private void tmrEnd_Tick(object sender, EventArgs e)
        {
            // Reset all of the lights
            ResetLights();

            // Disable this timer
            tmrEnd.Enabled = false;

            // We are finished counting
            Counting = false;
        }
    }
}
