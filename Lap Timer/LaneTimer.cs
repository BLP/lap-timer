﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.DirectInput;
using System.ComponentModel;
using System.Diagnostics;

namespace Lap_Timer
{
    public abstract class LaneTimer
    {
        public event EventHandler Reset;
        public event EventHandler TimingBegun;
        public event EventHandler TimingCancelled;
        public event EventHandler<LapCompletedEventArgs> LapCompleted;
        public event EventHandler<RaceCompletedEventArgs> RaceCompleted;
        protected Joystick TimerInput;
        protected BackgroundWorker Worker = new BackgroundWorker();
        protected TimeSpan LastTime;
        protected Stopwatch RaceTime;
        protected TimeSpan MinLapTime;
        protected int NumLaps;
        protected TimeSpan BestLap;
        protected TimeSpan LastLap;
        protected int ButtonNumber;

        public LaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime)
            : this(timerInput, buttonNumber, ref raceTime, TimeSpan.FromSeconds(1))
        {
        }

        public LaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime, TimeSpan minLapTime)
        {
            // Set the joystick input, button number, race timer, and minimum lap time
            TimerInput = timerInput;
            ButtonNumber = buttonNumber;
            RaceTime = raceTime;
            MinLapTime = minLapTime;

            // Set the best time to the maximum
            BestLap = TimeSpan.MaxValue;

            // Create the background worker
            Worker.DoWork += new DoWorkEventHandler(UpdateLane);
            Worker.WorkerSupportsCancellation = true;
        }

        public void SetTimer(Stopwatch raceTimer)
        {
            // Set the race time
            RaceTime = raceTimer;
        }

        public void BeginRace()
        {
            if (!Worker.IsBusy)
            {
                // Set the background worker to work if it isn't already
                Worker.RunWorkerAsync();

                // Notify everyone that timing is beginnng
                if (TimingBegun != null)
                    TimingBegun(this, new EventArgs());
            }
        }

        public virtual void ResetLane()
        {
            // Reset the info
            LastLap = TimeSpan.Zero;
            BestLap = TimeSpan.MaxValue;
            NumLaps = 0;

            // Notify everyone that a reset has occurred
            if (Reset != null)
                Reset(this, new EventArgs());
        }

        public void EndRace()
        {
            if (Worker.IsBusy)
            {
                // Stop the background worker from working
                Worker.CancelAsync();

                // Notify everyone that timing has been cancelled
                if (TimingCancelled != null)
                    TimingCancelled(this, new EventArgs());
            }
        }

        protected void InvokeLapCompleted(LapCompletedEventArgs e)
        {
            // Invoke the lap completed event
            if (LapCompleted != null)
                LapCompleted(this, e);
        }

        protected void InvokeRaceCompleted(RaceCompletedEventArgs e)
        {
            // Invoke the race completed event
            if (RaceCompleted != null)
                RaceCompleted(this, e);
        }

        protected abstract void UpdateLane(object sender, DoWorkEventArgs e);
    }

    public class RaceCompletedEventArgs : EventArgs
    {
        public int LapsCompleted { get; protected set; }
        public TimeSpan RaceTime { get; protected set; }
        public TimeSpan BestTime { get; protected set; }

        public RaceCompletedEventArgs(int lapsCompleted, TimeSpan raceTime, TimeSpan bestTime)
        {
            LapsCompleted = lapsCompleted;
            RaceTime = raceTime;
            BestTime = bestTime;
        }
    }

    public class LapCompletedEventArgs : EventArgs
    {
        public int LapsCompleted { get; protected set; }
        public TimeSpan LapTime { get; protected set; }
        public bool BestTime { get; protected set; }

        public LapCompletedEventArgs(int lapsCompleted, TimeSpan lapTime, bool bestTime)
        {
            LapsCompleted = lapsCompleted;
            LapTime = lapTime;
            BestTime = bestTime;
        }
    }
}