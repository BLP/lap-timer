﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.DirectInput;
using System.Diagnostics;
using System.ComponentModel;

namespace Lap_Timer
{
    public class LapsRaceLaneTimer : LaneTimer
    {
        protected int RaceLength;

        public LapsRaceLaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime, int raceLength)
            : base(timerInput, buttonNumber, ref raceTime)
        {
            RaceLength = raceLength;
        }

        public LapsRaceLaneTimer(Joystick timerInput, int buttonNumber, ref Stopwatch raceTime, TimeSpan minLapTime, int raceLength)
            : base(timerInput, buttonNumber, ref raceTime, minLapTime)
        {
            RaceLength = raceLength;
        }

        protected override void UpdateLane(object sender, DoWorkEventArgs e)
        {
            // Poll the joystick
            TimerInput.Poll();

            // Initialize joystick state
            JoystickState oldState = TimerInput.GetCurrentState();
            JoystickState newState;

            // Wait for the car to cross the line for the first time
            while (TimerInput.GetCurrentState().IsReleased(ButtonNumber) == oldState.IsReleased(ButtonNumber) || Worker.CancellationPending)
            {
                // Store the old joystick state
                oldState = TimerInput.GetCurrentState();

                // Poll the joystick
                TimerInput.Poll();
            }

            // Store the current time as the last time
            LastTime = RaceTime.Elapsed;

            // Loop until the race is finished, checking for the car to cross the line
            while (!Worker.CancellationPending)
            {
                // Poll the joystick
                TimerInput.Poll();

                // Get the current race time and the current joystick state
                TimeSpan currentTime = RaceTime.Elapsed;
                newState = TimerInput.GetCurrentState();

                if (newState.IsReleased(ButtonNumber) && oldState.IsPressed(ButtonNumber))
                {
                    // A car is newly crossing the line - calculate the lap time
                    TimeSpan lapTime = currentTime - LastTime;
                    if (lapTime > MinLapTime)
                    {
                        // The lap time isn't too short - a lap has been completed
                        NumLaps++;
                        LastLap = lapTime;

                        // Check for a best time
                        if (lapTime < BestLap)
                        {
                            // A new best time! Store it
                            BestLap = lapTime;

                            // Let any interested parties know that a new lap has been completed with a best time
                            InvokeLapCompleted(new LapCompletedEventArgs(NumLaps, LastLap, true));
                        }
                        else
                        {
                            // Let any interested parties know that a new lap has been completed without a best time
                            InvokeLapCompleted(new LapCompletedEventArgs(NumLaps, LastLap, false));
                        }

                        // Check to see if the race has been completed
                        if (NumLaps >= RaceLength)
                        {
                            // The race has been completed - fire the event
                            InvokeRaceCompleted(new RaceCompletedEventArgs(NumLaps, currentTime, BestLap));

                            // Break out of the loop
                            break;
                        }

                        // Store the current time as the last time
                        LastTime = currentTime;
                    }
                }

                // Store the previous joystick state
                oldState = newState;
            }

            // We're finished with timing, so dispose the worker
            Worker.Dispose();
        }
    }
}
