﻿namespace Lap_Timer
{
    partial class LapsRaceTimingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblRaceLength = new System.Windows.Forms.Label();
            this.Display4 = new Lap_Timer.TimingDisplay();
            this.Display3 = new Lap_Timer.TimingDisplay();
            this.Display2 = new Lap_Timer.TimingDisplay();
            this.Display1 = new Lap_Timer.TimingDisplay();
            this.Countdown = new Lap_Timer.CountdownLights();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(503, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 25);
            this.label16.TabIndex = 33;
            this.label16.Text = "Best Time";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(341, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 25);
            this.label15.TabIndex = 32;
            this.label15.Text = "Last Time";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(228, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 25);
            this.label14.TabIndex = 31;
            this.label14.Text = "Laps";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(636, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 25);
            this.label1.TabIndex = 42;
            this.label1.Text = "Place";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 25);
            this.label2.TabIndex = 43;
            this.label2.Text = "Race Length:";
            // 
            // lblRaceLength
            // 
            this.lblRaceLength.AutoSize = true;
            this.lblRaceLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRaceLength.Location = new System.Drawing.Point(158, 9);
            this.lblRaceLength.Name = "lblRaceLength";
            this.lblRaceLength.Size = new System.Drawing.Size(24, 25);
            this.lblRaceLength.TabIndex = 44;
            this.lblRaceLength.Text = "0";
            // 
            // Display4
            // 
            this.Display4.BackColor = System.Drawing.Color.Yellow;
            this.Display4.DisplayPlace = true;
            this.Display4.LaneNumber = 4;
            this.Display4.Location = new System.Drawing.Point(15, 288);
            this.Display4.Name = "Display4";
            this.Display4.Size = new System.Drawing.Size(694, 55);
            this.Display4.TabIndex = 41;
            this.Display4.Visible = false;
            // 
            // Display3
            // 
            this.Display3.BackColor = System.Drawing.Color.White;
            this.Display3.DisplayPlace = true;
            this.Display3.LaneNumber = 3;
            this.Display3.Location = new System.Drawing.Point(15, 217);
            this.Display3.Name = "Display3";
            this.Display3.Size = new System.Drawing.Size(694, 55);
            this.Display3.TabIndex = 40;
            this.Display3.Visible = false;
            // 
            // Display2
            // 
            this.Display2.BackColor = System.Drawing.Color.Blue;
            this.Display2.DisplayPlace = true;
            this.Display2.LaneNumber = 2;
            this.Display2.Location = new System.Drawing.Point(15, 146);
            this.Display2.Name = "Display2";
            this.Display2.Size = new System.Drawing.Size(694, 55);
            this.Display2.TabIndex = 39;
            this.Display2.Visible = false;
            // 
            // Display1
            // 
            this.Display1.BackColor = System.Drawing.Color.Red;
            this.Display1.DisplayPlace = true;
            this.Display1.LaneNumber = 1;
            this.Display1.Location = new System.Drawing.Point(15, 75);
            this.Display1.Name = "Display1";
            this.Display1.Size = new System.Drawing.Size(694, 55);
            this.Display1.TabIndex = 38;
            this.Display1.Visible = false;
            // 
            // Countdown
            // 
            this.Countdown.BackColor = System.Drawing.Color.Black;
            this.Countdown.Location = new System.Drawing.Point(17, 37);
            this.Countdown.Name = "Countdown";
            this.Countdown.Size = new System.Drawing.Size(124, 31);
            this.Countdown.TabIndex = 45;
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(147, 41);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(75, 23);
            this.btnStartStop.TabIndex = 46;
            this.btnStartStop.Text = "Begin Race";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // LapsRaceTimingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(726, 368);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.Countdown);
            this.Controls.Add(this.lblRaceLength);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Display4);
            this.Controls.Add(this.Display3);
            this.Controls.Add(this.Display2);
            this.Controls.Add(this.Display1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Name = "LapsRaceTimingForm";
            this.Text = "LapsRaceTimingForm";
            this.Load += new System.EventHandler(this.LapsRaceTimingForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TimingDisplay Display4;
        private TimingDisplay Display3;
        private TimingDisplay Display2;
        private TimingDisplay Display1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblRaceLength;
        private CountdownLights Countdown;
        private System.Windows.Forms.Button btnStartStop;
    }
}