﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SlimDX.DirectInput;
using System.Diagnostics;

namespace Lap_Timer
{
    public partial class TimingDisplay : UserControl
    {
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                lblLaneNumber.BackColor = value;
                lblLaps.BackColor = value;
                lblLast.BackColor = value;
                lblBest.BackColor = value;
                lblPlace.BackColor = value;
            }
        }

        private int laneNumber = 1;
        [Browsable(true)]
        public int LaneNumber
        {
            get
            {
                return laneNumber;
            }
            set
            {
                laneNumber = value;
                lblLaneNumber.Text = string.Format("Lane {0}", laneNumber);
            }
        }

        private bool displayPlace = true;
        [Browsable(true)]
        public bool DisplayPlace
        {
            get
            {
                return displayPlace;
            }
            set
            {
                displayPlace = value;
                lblPlace.Visible = value;
                if (displayPlace)
                {
                    Width = 694;
                }
                else
                {
                    Width = 615;
                }
            }
        }

        public TimingDisplay()
        {
            InitializeComponent();
        }

        public void LinkTimer(LaneTimer timer)
        {
            // Link the timer events
            timer.LapCompleted += new EventHandler<LapCompletedEventArgs>(timer_LapCompleted);
            timer.Reset += new EventHandler(timer_Reset);
            timer.TimingCancelled += new EventHandler(timer_TimingCancelled);
        }

        public void DelinkTimer(LaneTimer timer)
        {
            // Unhook the events
            timer.LapCompleted -= timer_LapCompleted;
            timer.Reset -= timer_Reset;
            timer.TimingCancelled -= timer_TimingCancelled;

            // Reset the display
            ResetDisplay();
        }

        public void SetPlace(int place)
        {
            // Set the place display
            lblPlace.Text = place.ToString();
        }

        private void timer_Reset(object sender, EventArgs e)
        {
            ResetDisplay();
        }

        private void ResetDisplay()
        {
            // Reset the lane display            
            lblLaps.Text = "0";
            lblLast.Text = String.Empty;
            lblBest.Text = String.Empty;
            lblPlace.Text = String.Empty;
        }

        private void UpdateDisplay(object sender, LapCompletedEventArgs e)
        {
            // Update the display for the number of laps completed and the last lap time
            lblLaps.Text = e.LapsCompleted.ToString();
            lblLast.Text = e.LapTime.TotalSeconds.ToString("0.00");
            if (e.BestTime)
            {
                // If the previous lap was a best lap, update the best lap display
                lblBest.Text = e.LapTime.TotalSeconds.ToString("0.00");
            }
        }

        private void timer_LapCompleted(object sender, LapCompletedEventArgs e)
        {
            // Create and invoke a delegate to update the display on the labels' own thread
            EventHandler<LapCompletedEventArgs> displayUpdate = new EventHandler<LapCompletedEventArgs>(UpdateDisplay);
            Invoke(displayUpdate, new object[] { sender, e });
        }

        void timer_TimingCancelled(object sender, EventArgs e)
        {
            // Delink the timer
            DelinkTimer((LaneTimer)sender);
        }
    }
}
