﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SlimDX.DirectInput;

namespace Lap_Timer
{
    public partial class LapsRaceTimingForm : Form
    {
        private int RaceLength;
        private Stopwatch raceTimer = new Stopwatch();
        private LapsRaceLaneTimer[] Timers;
        private TimingDisplay[] Displays = new TimingDisplay[4];

        public LapsRaceTimingForm(int raceLength)
        {
            InitializeComponent();
            Countdown.CountdownFinished += new EventHandler(Countdown_CountdownFinished);

            // Set the race length
            RaceLength = raceLength;

            // Create the array for storing lane timers
            Timers = new LapsRaceLaneTimer[TimingSettings.NumLanes];

            // Put the timing displays into the array
            Displays[0] = Display1;
            Displays[1] = Display2;
            Displays[2] = Display3;
            Displays[3] = Display4;
        }

        void Countdown_CountdownFinished(object sender, EventArgs e)
        {
            // Start timing
            raceTimer.Start();

            // Set the lane timers going
            for (int i = 0; i < Timers.Length; i++)
            {
                Timers[i].BeginRace();
            }
        }

        private void LapsRaceTimingForm_Load(object sender, EventArgs e)
        {
            // Display the length of the race
            lblRaceLength.Text = RaceLength.ToString();

            // Capture the joystick
            Joystick joystick = new Joystick(TimingSettings.Input, TimingSettings.TimingDevice.InstanceGuid);
            joystick.Acquire();
            joystick.Poll();

            // Link the timers to the displays
            for (int i = 0; i < TimingSettings.NumLanes; i++)
            {
                // Create the lane timer and set it to the correct button
                Timers[i] = new LapsRaceLaneTimer(joystick, TimingSettings.LaneButtons[i], ref raceTimer, TimingSettings.MinimumLapTime,
                    RaceLength);

                // Link the timer to the proper display
                Displays[i].LinkTimer(Timers[i]);

                // Set the display back color and visibility
                Displays[i].BackColor = TimingSettings.LaneColors[i];
                Displays[i].Visible = true;
            }
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (btnStartStop.Text == "Begin Race")
            {
                // Start the countdown
                Countdown.BeginCounting();

                // Change this to a cancel button
                btnStartStop.Text = "End Race";
            }
            else if (btnStartStop.Text == "End Race")
            {
                // We are cancelling the race
                if (Countdown.Counting)
                {
                    // Cancel the countdown
                    Countdown.CancelCounting();
                }
                else
                {
                    // Stop the timers from timing
                    for (int i = 0; i < Timers.Length; i++)
                    {
                        Timers[i].EndRace();
                    }

                    // Stop the race timer
                    raceTimer.Stop();
                }
                // Disable this button
                btnStartStop.Enabled = false;
            }
        }
    }
}
