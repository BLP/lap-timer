﻿namespace Lap_Timer
{
    partial class PracticeTimingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnReset4 = new System.Windows.Forms.Button();
            this.btnReset2 = new System.Windows.Forms.Button();
            this.btnReset1 = new System.Windows.Forms.Button();
            this.btnReset3 = new System.Windows.Forms.Button();
            this.Display4 = new Lap_Timer.TimingDisplay();
            this.Display3 = new Lap_Timer.TimingDisplay();
            this.Display2 = new Lap_Timer.TimingDisplay();
            this.Display1 = new Lap_Timer.TimingDisplay();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(225, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 25);
            this.label14.TabIndex = 16;
            this.label14.Text = "Laps";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(338, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 25);
            this.label15.TabIndex = 17;
            this.label15.Text = "Last Time";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(500, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 25);
            this.label16.TabIndex = 18;
            this.label16.Text = "Best Time";
            // 
            // btnReset4
            // 
            this.btnReset4.Location = new System.Drawing.Point(633, 301);
            this.btnReset4.Name = "btnReset4";
            this.btnReset4.Size = new System.Drawing.Size(75, 23);
            this.btnReset4.TabIndex = 26;
            this.btnReset4.Text = "Reset";
            this.btnReset4.UseVisualStyleBackColor = true;
            this.btnReset4.Visible = false;
            this.btnReset4.Click += new System.EventHandler(this.btnReset4_Click);
            // 
            // btnReset2
            // 
            this.btnReset2.Location = new System.Drawing.Point(633, 159);
            this.btnReset2.Name = "btnReset2";
            this.btnReset2.Size = new System.Drawing.Size(75, 23);
            this.btnReset2.TabIndex = 24;
            this.btnReset2.Text = "Reset";
            this.btnReset2.UseVisualStyleBackColor = true;
            this.btnReset2.Visible = false;
            this.btnReset2.Click += new System.EventHandler(this.btnReset2_Click);
            // 
            // btnReset1
            // 
            this.btnReset1.Location = new System.Drawing.Point(633, 88);
            this.btnReset1.Name = "btnReset1";
            this.btnReset1.Size = new System.Drawing.Size(75, 23);
            this.btnReset1.TabIndex = 23;
            this.btnReset1.Text = "Reset";
            this.btnReset1.UseVisualStyleBackColor = true;
            this.btnReset1.Visible = false;
            this.btnReset1.Click += new System.EventHandler(this.btnReset1_Click);
            // 
            // btnReset3
            // 
            this.btnReset3.Location = new System.Drawing.Point(633, 230);
            this.btnReset3.Name = "btnReset3";
            this.btnReset3.Size = new System.Drawing.Size(75, 23);
            this.btnReset3.TabIndex = 25;
            this.btnReset3.Text = "Reset";
            this.btnReset3.UseVisualStyleBackColor = true;
            this.btnReset3.Visible = false;
            this.btnReset3.Click += new System.EventHandler(this.btnReset3_Click);
            // 
            // Display4
            // 
            this.Display4.BackColor = System.Drawing.Color.Yellow;
            this.Display4.DisplayPlace = false;
            this.Display4.LaneNumber = 4;
            this.Display4.Location = new System.Drawing.Point(12, 285);
            this.Display4.Name = "Display4";
            this.Display4.Size = new System.Drawing.Size(615, 55);
            this.Display4.TabIndex = 30;
            this.Display4.Visible = false;
            // 
            // Display3
            // 
            this.Display3.BackColor = System.Drawing.Color.White;
            this.Display3.DisplayPlace = false;
            this.Display3.LaneNumber = 3;
            this.Display3.Location = new System.Drawing.Point(12, 214);
            this.Display3.Name = "Display3";
            this.Display3.Size = new System.Drawing.Size(615, 55);
            this.Display3.TabIndex = 29;
            this.Display3.Visible = false;
            // 
            // Display2
            // 
            this.Display2.BackColor = System.Drawing.Color.Blue;
            this.Display2.DisplayPlace = false;
            this.Display2.LaneNumber = 2;
            this.Display2.Location = new System.Drawing.Point(12, 143);
            this.Display2.Name = "Display2";
            this.Display2.Size = new System.Drawing.Size(615, 55);
            this.Display2.TabIndex = 28;
            this.Display2.Visible = false;
            // 
            // Display1
            // 
            this.Display1.BackColor = System.Drawing.Color.Red;
            this.Display1.DisplayPlace = false;
            this.Display1.LaneNumber = 1;
            this.Display1.Location = new System.Drawing.Point(12, 72);
            this.Display1.Name = "Display1";
            this.Display1.Size = new System.Drawing.Size(615, 55);
            this.Display1.TabIndex = 27;
            this.Display1.Visible = false;
            // 
            // PracticeTimingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(726, 368);
            this.Controls.Add(this.Display4);
            this.Controls.Add(this.Display3);
            this.Controls.Add(this.Display2);
            this.Controls.Add(this.Display1);
            this.Controls.Add(this.btnReset4);
            this.Controls.Add(this.btnReset3);
            this.Controls.Add(this.btnReset2);
            this.Controls.Add(this.btnReset1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Name = "PracticeTimingForm";
            this.Text = "Simple Lap Timer v1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnReset4;
        private System.Windows.Forms.Button btnReset2;
        private TimingDisplay Display1;
        private System.Windows.Forms.Button btnReset1;
        private TimingDisplay Display2;
        private System.Windows.Forms.Button btnReset3;
        private TimingDisplay Display3;
        private TimingDisplay Display4;
    }
}

